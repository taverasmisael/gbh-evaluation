import PushOverMenu from './PushOverMenu'

function InitView() {
  const PageWrapper = document.getElementById('PageWrapper')
  const NavContainer = document.querySelector('.nav')
  const NavOpeners = document.querySelectorAll('.nav__open')
  const NavClosers = document.querySelectorAll('.nav__close')

  const MainNav = new PushOverMenu('wrapper--open', PageWrapper, NavContainer)
  NavOpeners.forEach(el => el.addEventListener('click', MainNav.open))
  NavClosers.forEach(el => el.addEventListener('click', MainNav.close))

  // Only Load MoveTo if is needed
  if (!document.location.pathname.includes('get-started')) {
    import(/* webpackChunkName: "moveTo" */'moveTo').then(({ default: MoveTo }) => {
      const moveTo = new MoveTo({ tolerance: 50 })
      document.querySelectorAll('a[href^="#"]').forEach(a => moveTo.registerTrigger(a))
    })
  }
}
document.addEventListener('DOMContentLoaded', InitView)
