export default class PushOverMenu {
  isOpen = false
  open = () => {
    this.isOpen = true
    this.wrapper.classList.add(this.activeClass)
    this.focusableElements.forEach(this.setTabIndex('0'))
    this.nav.removeAttribute('aria-hidden')
    this.focusedBeforeOpen = document.activeElement
    this.nav.addEventListener('keydown', this.handleNavKeyDown)
    this.firstFocusableElement.focus()
  }

  close = () => {
    this.isOpen = false
    this.wrapper.classList.remove(this.activeClass)
    this.wrapper.querySelector('main').focus()
    this.nav.setAttribute('aria-hidden', true)
    this.focusableElements.forEach(this.setTabIndex('-1'))
    if (this.focusedBeforeOpen) this.focusedBeforeOpen.focus()
  }
  toggle = () => {
    if (this.wrapper.classList.contains(this.activeClass)) {
      this.close()
    } else {
      this.open()
    }
  }

  handleBackwardTab = (event) => {
    if (document.activeElement === this.firstFocusableElement) {
      event.preventDefault()
      this.lastFocusableElement.focus()
    }
  }
  handleForwardTab = (event) => {
    if (document.activeElement === this.lastFocusableElement) {
      event.preventDefault()
      this.firstFocusableElement.focus()
    }
  }

  handleNavKeyDown = (event) => {
    const { keyCode, shiftKey } = event
    if (keyCode === 27) this.close()
    if (keyCode === 9) {
      if (this.focusableElements.length === 1) {
        event.preventDefault()
      }
      if (shiftKey) {
        this.handleBackwardTab(event)
      } else {
        this.handleForwardTab(event)
      }
    }
  }

  setTabIndex = value => el => el.setAttribute('tabindex', value)

  onDOMClick = () => {
    this.wrapper.addEventListener('click', ({ target }) => {
      const isClickInside = this.nav.contains(target)
      const isOpener = target.classList.contains('nav__open')
      if (!isClickInside && !isOpener && this.isOpen) this.close()
    })
  }
  constructor(activeClass, wrapper, nav) {
    this.activeClass = activeClass
    this.wrapper = wrapper
    this.nav = nav
    this.focusableElements = nav.querySelectorAll('.nav__link, .nav__closing, a')
    this.firstFocusableElement = this.focusableElements[0]
    this.lastFocusableElement = this.focusableElements[this.focusableElements.length - 1]
    this.onDOMClick()
  }
}
