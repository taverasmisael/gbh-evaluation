/* eslint import/no-extraneous-dependencies: 0 */
import Jarvis from 'webpack-jarvis'
import merge from 'webpack-merge'

import common from './webpack.common'

export default merge(common, {
  devtool: 'source-map',
  stats: {
    colors: true,
    reasons: true
  },
  plugins: [new Jarvis()]
})
